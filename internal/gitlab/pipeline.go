package gitlab

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

// JobVariableType a CI/CD variable can be of different types.
type JobVariableType string

const (
	// EnvironmentVariableType represents a SHELL environment variable.
	EnvironmentVariableType JobVariableType = "env_var"
	// FileVariableType will store the contents of the variable inside of a
	// file, and the specified KEY will have the value of the PATH of the file.
	FileVariableType JobVariableType = "file"
)

// JobVariable represents a CI/CD variable for the api
type JobVariable struct {
	Key   string          `json:"key"`
	Type  JobVariableType `json:"variable_type"`
	Value string          `json:"value"`
}

type createPipelineRequest struct {
	Ref       string        `json:"ref"`
	Variables []JobVariable `json:"variables"`
}

type createPipelineResponse struct {
	WebURL string `json:"web_url"`
}

// CreatePipeline creates a new pipeline for the specified project.
func (c *Client) CreatePipeline(ctx context.Context, projectID int, variables []JobVariable) (string, error) {
	createPipelineReq := createPipelineRequest{
		Ref:       "main",
		Variables: variables,
	}

	b, err := json.Marshal(createPipelineReq)
	if err != nil {
		return "", fmt.Errorf("marshal createPipelineRequest to JSON #%+v: %w", createPipelineReq, err)
	}

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodPost,
		fmt.Sprintf("%s/projects/%d/pipeline", c.restEndpoint, projectID),
		bytes.NewReader(b),
	)
	if err != nil {
		return "", fmt.Errorf("create http request for pipeline create: %w", err)
	}

	req.Header.Set(c.restAPIAuthHeader())
	req.Header.Set("Content-Type", "application/json")

	resp, err := c.restAPI.Do(req)
	if err != nil {
		return "", fmt.Errorf("create pipeline request: %w", err)
	}

	defer func() {
		closeErr := resp.Body.Close()
		if closeErr != nil {
			c.logger.Error(err, "Closing body for pipeline request")
		}
	}()

	if resp.StatusCode != http.StatusCreated {
		return "", fmt.Errorf("create pipeline request not 201 Create: %d", resp.StatusCode)
	}

	pipeline := createPipelineResponse{}

	err = json.NewDecoder(resp.Body).Decode(&pipeline)
	if err != nil {
		return "", fmt.Errorf("decoding create pipline response body: %w", err)
	}

	return pipeline.WebURL, nil
}
