package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/bombsimon/logrusr"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-runner-stress/gitlab-runner-stress/internal/gitlab/testdata"
)

func TestClient_CreatePipeline(t *testing.T) {
	tests := map[string]struct {
		expectedURL        string
		responseStatusCode int
		response           string
		slowResponse       bool
		expectedErr        bool
	}{
		"200 Created": {
			expectedURL:        "https://example.com/foo/bar/pipelines/61",
			responseStatusCode: http.StatusCreated,
			response:           testdata.PipelineCreateSuccessResp,
		},
		"500 Internal Server Error": {
			responseStatusCode: http.StatusInternalServerError,
			expectedErr:        true,
		},
		"invalid response": {
			responseStatusCode: http.StatusOK,
			response:           "junk",
			expectedErr:        true,
		},
		"timeout": {
			responseStatusCode: http.StatusOK,
			slowResponse:       true,
			expectedErr:        true,
		},
	}

	for testName, tt := range tests {
		testVar := JobVariable{
			Key:   "TEST",
			Type:  EnvironmentVariableType,
			Value: "true",
		}

		t.Run(testName, func(t *testing.T) {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				require.Equal(t, testToken, r.Header.Get("PRIVATE-TOKEN"))
				require.Equal(t, "/api/v4/projects/123/pipeline", r.URL.String())
				require.Equal(t, "application/json", r.Header.Get("Content-Type"))

				b, err := ioutil.ReadAll(r.Body)
				require.NoError(t, err)

				expectedJSONBody := createPipelineRequest{
					Ref:       "main",
					Variables: []JobVariable{testVar},
				}
				eb, err := json.Marshal(expectedJSONBody)
				require.NoError(t, err)
				require.Equal(t, eb, b)

				if tt.slowResponse {
					time.Sleep(100 * time.Millisecond)
				}

				w.WriteHeader(tt.responseStatusCode)
				fmt.Fprint(w, tt.response)
			}))
			defer ts.Close()

			client := NewClient(logrusr.NewLogger(logrus.New()), testToken, ts.URL)
			client.restAPI = &http.Client{Timeout: 50 * time.Millisecond}

			jobVariables := []JobVariable{testVar}

			url, err := client.CreatePipeline(context.Background(), 123, jobVariables)
			require.Equal(t, tt.expectedErr, err != nil)
			require.Equal(t, tt.expectedURL, url)
		})
	}
}
