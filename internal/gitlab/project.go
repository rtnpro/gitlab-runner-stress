package gitlab

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"github.com/machinebox/graphql"
)

// projectGIDPrefix is also added in front of the project ID when using the
// `project` query in GraphQL.
const projectGIDPrefix = "gid://gitlab/Project/"

type projectQueryResponse struct {
	Project struct {
		ID string `json:"id"`
	} `json:"project"`
}

// ProjectID gets the GitLab ID for the specified path.
func (c *Client) ProjectID(ctx context.Context, path string) (int, error) {
	req := graphql.NewRequest(`
		query ($path: ID!) {
  			project(fullPath: $path) {
    			id
  			}
		}
	`)

	req.Var("path", path)
	req.Header.Set(c.graphQLAuthHeader())

	resp := projectQueryResponse{}

	err := c.graphQL.Run(ctx, req, &resp)
	if err != nil {
		return 0, fmt.Errorf("sending graphQL query: %w", err)
	}

	projectGID := strings.TrimPrefix(resp.Project.ID, projectGIDPrefix)

	projectID, err := strconv.Atoi(projectGID)
	if err != nil {
		return 0, fmt.Errorf("converting project gid %q id to int: %w", resp.Project.ID, err)
	}

	return projectID, nil
}
